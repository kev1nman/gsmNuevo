<?php
error_reporting(E_ALL ^ E_NOTICE);
    function cabecera(){echo "
        <!DOCTYPE html>
        <html lang='en'>
        <head>
            <meta charset='UTF-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1.0'>
            <meta http-equiv='X-UA-Compatible' content='ie=edge'>

                <!--Bootstrap CSS-->
                    <link rel='stylesheet' href='css/bootstrap.min.css'>
                <!--Estilos-->
                    <link rel='stylesheet' href='css/iconStyle.css'>
                <!--Estilos-->
                    <link rel='stylesheet' href='css/sly.css'>
                <!--Estilos-->
                    <link rel='stylesheet' href='css/style.css'>
                <!--Jquery-->
                    <script src='js/jquery.js' type='text/javascript'></script>
                <!--Jquery-->
                    <script src='js/bootstrap.min.js' type='text/javascript'></script>
                <!--SLY - Plugin Slider -->
                    <script src='js/plugins.js' type='text/javascript'></script>
                <!--SLY - sly Slider -->
                    <script src='js/sly.min.js' type='text/javascript'></script>
                <!--SLY - horizontal Slider -->
                    <script src='js/horizontal.js' type='text/javascript'></script>

            <title>GSM ABROAD</title>
        </head>
        <body>
            <!-- GLOBAL -->
            <div class='global'>
                <!-- MENU -->
                <nav class='navbar navbar-default navbar-fixed-top'>
                    <!-- CONTAINER -->
                    <div class='container-fluid'>
                        <!-- Responsive -->
                        <div class='navbar-header'>
                            <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
                                <span class='sr-only'>Toggle navigation</span>
                                <span class='icon-bar'></span>
                                <span class='icon-bar'></span>
                                <span class='icon-bar'></span>
                            </button>
                            <div class='logo'>
                                <a class='navbar-brand' href='index.php'>
                                    <img class='img-responsive imgLogo' src='img/logo.png' alt=''>
                                </a>
                            </div>
                            <div class='redesFlotantes'>
                                <a href='#'><span class='icon-facebook2'></span></a>
                                <a href='#'><span class='icon-twitter'></span></a>
                                <a href='#'><span class='icon-google-plus'></span></a>
                            </div>
                        </div><!-- FIN / Responsive -->

                        <!-- Botones -->
                        <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
                            <!-- Search - Login - Register-->
                            <div class='navbar-right col-md-5'>
                                <!-- Login - Register -->
                                <div class='login2'>
                                    <a class='cuadrado' href='#'><span class='icon icon-user circular'></span></a>
                                    <a class='cuadrado' href='register.php'><span class='icon-user-plus circular circular2'></span></a>
                                </div><!-- FIN / Login - Register -->
                                <!-- Caja Busqueda -->
                                <form class='navbar-form col-md-12 form-inline' role='form'>
                                    <div class='form-group has-default has-feedback'>
                                        <input type='text' class='search form-control' placeholder='Phone Model or IMEI'>
                                        <span class='glyphicon glyphicon-search form-control-feedback text-muted'></span>
                                    </div>
                                </form><!-- FIN / Caja Busqueda -->
                            </div><!-- FIN / Search - Login - Register-->

                            <div class='col-sm-6 col-md-7 noPadding menu'>
                                <ul class='nav navbar-nav'>
                                    <li class='dropdown'>
                                        <a href='smarthphones.php' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Smarthphones <span class='caret'></span></a>
                                        <ul class='dropdown-menu'>
                                            <li><a href='unlockCellphone.php'>Unlock your Cellphone</a></li>
                                            <li><a href='unlockFAQ.php'>Unlock FAQ</a></li>
                                        </ul>
                                    </li>
                                    <li class='dropdown'>
                                        <a href='smarthphones.php' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Whosale <span class='caret'></span></a>
                                        <ul class='dropdown-menu'>
                                            <li><a href='register.php'>Register New Account</a></li>
                                            <li><a href='WholeSale.php'>Wholesales Unlock Prices</a></li>
                                            <li><a href='suppliers.php'>Become a supplier</a></li>
                                        </ul>
                                    </li>
                                    <li class='dropdown'>
                                        <a href='smarthphones.php' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Legal <span class='caret'></span></a>
                                        <ul class='dropdown-menu'>
                                            <li><a href='privacyPolicy.php'>Privacy Policy</a></li>
                                            <li><a href='termsConditions.php'>Terms & Conditions</a></li>
                                            <li><a href='termsOfUse.php'>Terms of Use</a></li>
                                            <li><a href='contact.php'>Contact Us</a></li>
                                            <li><a href='aboutUs.php'>About Us</a></li>
                                        </ul>
                                    </li>
                                    <li><a href='faqs.php'>FAQS</a></li>
                                    <li><a href='#'>Feed back</a></li>
                                    <li><a href='contact.php'>Contact US</a></li>
                                </ul>
                            </div>
                            <!-- Login - Register -->
                            <div class='col-md-4 login'>
                                <a class='cuadrado' href='#'><span class='icon icon-user circular'></span> Login</a>
                                <a class='cuadrado' href='register.php'><span class='icon-user-plus circular circular2'></span> Register</a>
                            </div><!-- FIN / Login - Register -->


                        </div><!-- FIN / Botones -->
                    </div><!-- FIN / CONTAINER-FLUID -->
                </nav><!-- FIN / MENU -->
    ";}

    function footer(){
        echo "
        <script type='text/javascript'>
        $(function(){
            $('.dropdown').hover(function() {
                $(this).addClass('open');
            },
            function() {
                $(this).removeClass('open');
            });
        });
        </script>
            <!-- FOOTER -->
            <footer>
                <!-- LISTAS -->
                <div class='container links'>
                    <div class='row'>
                        <div class='col-xs-6 col-sm-3 col-md-3'>
                            <div class='col-md-12'>
                                <h5><span class='icon-unlocked'></span> SMARTHPHONES</h5>
                                <ul>
                                    <li><a href='unlockCellphone.php'><span class='icon-circle-right'></span> Unlock your cellphone</a></li>
                                    <li><a href='unlockFAQ.php'><span class='icon-circle-right'></span> Unlock faq</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class='col-xs-6 col-sm-3 col-md-3'>
                            <div class='col-md-12'>
                                <h5><span class='icon-unlocked'></span> WHOLESALE</h5>
                                <ul>
                                    <li><a href='register.php'><span class='icon-circle-right'></span> Register New Account</a></li>
                                    <li><a href='WholeSale.php'><span class='icon-circle-right'></span> Wholesales Unlock Prices</a></li>
                                    <li><a href='suppliers.php'><span class='icon-circle-right'></span> Become a supplier</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class='col-xs-6 col-sm-3 col-md-3'>
                            <div class='col-md-12'>
                                <h5><span class='icon-info'></span> LEGAL</h5>
                                <ul>
                                    <li><a href='privacyPolicy.php'><span class='icon-circle-right'></span> Privacy Policy</a></li>
                                    <li><a href='termsConditions.php'><span class='icon-circle-right'></span> Terms & Conditions</a></li>
                                    <li><a href='termsOfUse.php'><span class='icon-circle-right'></span> Terms of Use</a></li>
                                    <li><a href='contact.php'><span class='icon-circle-right'></span> Contact Us</a></li>
                                    <li><a href='aboutUs.php'><span class='icon-circle-right'></span> About Us</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class='col-xs-6 col-sm-3 col-md-3'>
                            <div class='col-md-12'>
                                <h5><span class='icon-bubble'></span> FEED BACK</h5>
                                <ul>
                                </ul>
                            </div>
                        </div>

                        <div class='col-xs-6 col-sm-3 col-md-3'>
                            <div class='col-md-12'>
                                <h5><span class='icon-mail3'></span> Contact US</h5>
                                <p>email us at: contac@email.com <br>
                                Phone: 58 999 12 12</p>
                            </div>
                        </div>
                    </div>
                </div><!-- FIN / LISTAS -->

                <!-- copyright -->
                <div class='copyright'>
                    <div class='container'>
                        <div class='col-md-6 text-center'>
                            <p>© 2016 - All Rights with Webenlance</p>
                        </div>
                        <div class='col-md-6 text-center'>
                            <a href='#'><span class='social icon-facebook2'></span></a>
                            <a href='#'><span class='social icon-twitter'></span></a>
                            <a href='#'><span class='social icon-google'></span></a>
                            <a href='#'><span class='social icon-youtube'></span></a>
                            <a href='#'><span class='social icon-skype'></span></a>
                            <a href='#'><span class='social icon-envelop'></span></a>
                        </div>
                    </div>
                </div><!-- FIN / copyright -->

            </footer>
            <!-- FIN FOOTER -->

        </div><!-- FIN / CONTENIDO -->
        </div><!-- FIN / GLOBAL -->



        </body>
        </html>
    ";}

 ?>
