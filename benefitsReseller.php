<?php
    require_once("master.php");
    cabecera();
 ?>

<!-- CONTENIDO -->
<div class="contenido">
    <div class="container">

        <div class="col-md-10 col-md-offset-1">
            <h1></h1>
            <div class="col-md-6">
                An Unlocked phone worth more than a locked one. Make more money reselling it!
            </div>
            <div class="col-md-6">
                Unlocking any mobile Device is a 100% legal activity approved by government. Furthermore your device will not lose Factory guarantee.
            </div>
            <div class="col-md-6">
                No technical knowledge needed. Only needs the Unlock Code and instructions that we send you when order is processed.
            </div>
            <div class="col-md-6">
                Unlocking your phone gives you the freedom of using your phone with any service provider!
            </div>
            <div class="col-md-6">
                Use your Mobile anywhere. GSM service Abroad will be unlimited in your Unlocked device!
            </div>
            <div class="col-md-6">
                No more Roaming fees. Use your phone any time you need with any worldwide carrier.
            </div>
            <div class="col-md-6">
                We have several ways to contact us if you have any question. You will receive an accurate answer in less than 24Hrs!!
            </div>
            <div class="col-md-6">
                Our website is totally transaction secured keeping your personal business information safely and private.
            </div>
            <div class="col-md-6">
                Don’t need to worry about your money. If you meet service terms, we always grant full refund of your purchase.
            </div>
            <div class="col-md-6">
                In order to keep efficiency and efficacy in all your orders we have made all order process an experience easy to understand to monetize your business.
            </div>
            <div class="col-md-6">
                If you find a cheaper price from our competitor but you still want to use GSM Fusion services, please feel free to contact us. All our advertised prices can be negotiated and we can always beat the competition.
            </div>
            <div class="col-md-6">
                If you have lower prices, just contact us to negotiate our advertised prices for your needs. We always beat any real price.
            </div>
            <div class="col-md-6">
                GSM Fusion has unlocked over 4 million mobile phones in 154 countries. By combining a reliable product with easy to follow instructions, we have arrived a proven formula.
            </div>
            <div class="col-md-6">
                GSM Abroad has unlocked millions of phones worldwide. We are your best source for your Unlock needs. We supply solutions for all type of customers.
            </div>

        </div>
    </div>

    <?php
    footer();
 ?>
