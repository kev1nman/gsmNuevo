<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <section class="registerBox">
                <div class="container">
                  <div class="col-md-6 col-md-offset-3">
                    <!-- contactBox -->
                    <form data-toggle="validator" role="form">
                      <h1>Register</h1>
                      <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="myname@email.com" data-error="Bruh, that email address is invalid" focus required>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label" for="nombres">First name</label>
                          <input type="text" name="nombres" class="form-control" id="inputName" placeholder="First name" required>
                        </div>

                        <div class="form-group">
                          <label for="inputName" class="control-label" for="apellidos">Last name</label>
                          <input type="text" name="apellidos" class="form-control" id="inputName" placeholder="Last name" required>
                        </div>

                        <div class="form-group">
                          <label for="telefono" class="control-label" for="telefono">Phone</label>
                          <input type="numbe" name="telefono" class="form-control" id="inputName" placeholder="Phone" required>
                        </div>

                        <div class="form-group">
                          <label for="direccion" class="control-label" for="direccion">Address</label>
                          <input type="text" name="direccion" class="form-control" id="inputName" placeholder="Address" required>
                        </div>

                      </div>
                      <div class="col-md-6">

                        <div class="form-group">
                          <label for="pais" class="control-label" for="pais">Country</label>
                          <input type="text" name="pais" class="form-control" id="inputName" placeholder="Country" required>
                        </div>

                        <div class="form-group">
                          <label for="estado" class="control-label" for="estado">State</label>
                          <input type="text" name="estado" class="form-control" id="inputName" placeholder="State" required>
                        </div>

                        <div class="form-group">
                          <label for="ciudad" class="control-label" for="pais">City</label>
                          <input type="text" name="pais" class="form-control" id="inputName" placeholder="City" required>
                        </div>

                        <div class="form-group">
                          <label for="zip" class="control-label" for="zip">Zip code</label>
                          <input type="text" name="zip" class="form-control" id="inputName" placeholder="Zip code" required>
                        </div>
                      </div>




                      <div class="form-group text-center">
                        <button type="submit" class="btn btn-success btn-lg">Suscribe</button>
                      </div>

                    </form>

                  </div>

                </div>

            </section>
        
<?php
    footer();
 ?>
