<?php
    require_once("master.php");
    cabecera();
 ?>

<!-- CONTENIDO -->
<div class="contenido">
    <div class="container">

        <div class="col-md-10 col-md-offset-1">
            <h1>Privacy Policy</h1>
            <ol>
                <h3><li>Audience</li></h3>
                <p>
                    This policy applies to all visitors to our website, registered users as well as customers who have subscribed to the services we offer.

                </p>
                <h3><li>Overview</li></h3>
                <p>
                    We are committed to protecting the privacy of our users, and strives to provide a safe, secure user experience. This Privacy Statement sets forth the online data collection and usage policies and practices that apply to this web site. The following statement
                    explains our commitment to managing your personal information and sets out how and when personal information is collected, held, treated, used or disclosed.

                </p>
                <h3><li>Acceptance of terms</li></h3>
                <p>
                    You must accept all the terms of this policy when you register on our website. If you do not agree with anything in this policy, then you may not register or use any of the services. You may not access our website or use our services if you are younger
                    than 13 years old. Users under 13 years are ONLY allowed to use the website if accompanied by a parent.

                </p>
                <h3><li>Collecting personal information</li></h3>
                <p>
                    We may collect, store and use the following kinds of personal information:

                </p>
                <ul>
                    <li>Information about your computer and about your visits to and use of this website (including your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website
                        navigation paths);</li>
                    <li>Information that you provide to us when registering with our website (including your name, email address, phone number, country, billing information and other information needed to identify you);</li>
                    <li>Information that you provide when completing your profile on our website (including your username, password, profile pictures and gender);</li>
                    <li>Information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters (including your email address);</li>
                    <li>Information that you provide to us when using the services on our website, or that is generated in the course of the use of those services (including the timing, frequency and pattern of service use);</li>
                    <li>Information relating to any purchases you make of our services or any other transactions that you enter into through our website (including your name, address, telephone number, email address and card details);</li>
                    <li>Information contained in or relating to any communication that you send to us or send through our website (including the communication content and metadata associated with the communication);</li>
                    <li>Any other personal information that you choose to send to us.</li>
                </ul>
                <p>
                    Before you disclose to us the personal information of another person, you must obtain that person's consent to both the disclosure and the processing of that personal information in accordance with this policy.
                </p>
                <h3><li>Using personal information</li></h3>
                <p>
                    Personal information submitted to us through our website will be used for the purposes specified in this policy or on the relevant pages of the website.

                </p>
                <p>
                    We may use your personal information to:
                </p>
                <ul>
                    <li>Administer our website and business;</li>
                    <li>Personalise our website for you;</li>
                    <li>Enable your use of the services available on our website;</li>
                    <li>Supply to you services purchased through our website;</li>
                    <li>Send statements, invoices and payment reminders to you, and collect payments from you;</li>
                    <li>Send you non-marketing commercial communications;</li>
                    <li>Send you email notifications that you have specifically requested;</li>
                    <li>Send you our email newsletter, if you have requested it (you can inform us at any time if you no longer require the newsletter);</li>
                    <li>Send you marketing communications relating to our business or the businesses of carefully-selected third parties which we think may be of interest to you, by post or, where you have specifically agreed to this, by email or similar
                        technology (you can inform us at any time if you no longer require marketing communications);</li>
                    <li>Provide third parties with statistical information about our users (but those third parties will not be able to identify any individual user from that information);</li>
                    <li>Deal with enquiries and complaints made by or about you relating to our website;</li>
                    <li>Keep our website secure and prevent fraud;</li>
                    <li>Verify compliance with the terms and conditions governing the use of our website; and</li>
                    <li>Other uses.</li>
                </ul>

                <p>
                    If you submit personal information for publication on our website, we will publish and otherwise use that information in accordance with the licence you grant to us.

                </p>
                <p>
                    We will not supply your personal information to any third party for the purpose of their or any other third party's direct marketing.

                </p>
                <p>
                    All our website financial transactions are handled through our payment services provider, PayPal, Skrill, Western Union and MoneyGram. You can review the provider's privacy policy at

                </p>
                <p>
                    PayPal: <a href="https://www.paypal.com/ie/webapps/mpp/ua/privacy-full">https://www.paypal.com/ie/webapps/mpp/ua/privacy-full</a> <br>
                    Skrill: <a href="https://www.skrill.com/en/footer/privacy-policy">https://www.skrill.com/en/footer/privacy-policy</a> <br>
                    WesternUnion: <a href="http://www.westernunion.ca/WUCOMWEB/staticMid.do?method=load&pagename=privacyPolicy">http://www.westernunion.ca/WUCOMWEB/staticMid.do?method=load&pagename=privacyPolicy</a> <br>
                    MoneyGram: <a href="http://global.moneygram.com/privacypolicies">http://global.moneygram.com/privacypolicies</a><br>
                </p>
                <p>
                    We will share information with our payment services provider only to the extent necessary for the purposes of processing payments you make via our website, refunding such payments and dealing with complaints and queries relating to such payments and refunds.
                </p>

                <h3><li>Disclosing personal information</li></h3>
            	<p>
            	    We may disclose your personal information:
            	</p>
                <ul>
                    <li>to the extent that we are required to do so by law;</li>
                    <li>in connection with any ongoing or prospective legal proceedings;</li>
                    <li>in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk);</li>
                    <li>to any person who we reasonably believe may apply to a court or other competent authority for disclosure of that personal information where, in our reasonable opinion, such court or authority would be reasonably likely to order disclosure of that personal information.</li>
                    Except as provided in this policy, we will not provide your personal information to third parties.</li>
                </ul>
                <h3><li>Retaining personal information</li></h3>
                <p>
                    This Section sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal information.
                </p>
                <p>
                    Personal information that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.
                </p>
                <p>
                    Without prejudice to the statement above, we will usually delete all users’ accounts that are inactive for long time as well as personal data related with such accounts.
                </p>
                <p>
                    Notwithstanding the other provisions of this Section, we will retain documents (including electronic documents) containing personal data:
                </p>
                <ul>
                    <li>to the extent that we are required to do so by law;</li>
                    <li>if we believe that the documents may be relevant to any ongoing or prospective legal proceedings; and</li>
                    <li>in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk).</li>
                </ul>

                <h3><li>Security of personal information</li></h3>
                <p>
                    We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information. We will store all the personal information you provide on our secure (password- and firewall-protected) servers. All electronic financial transactions entered into through our website will be protected by encryption technology.
                </p>
                <p>
                    However, you acknowledge that the transmission of information over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.
                </p>
                <p>
                    You are responsible for keeping the password you use for accessing our website confidential; we will not ask you for your password (except when you log in to our website).
                </p>

                <h3><li>Amendments</li></h3>
                <p>
                    We may update this policy from time to time by publishing a new version on our website. As such, you are advised to check this page occasionally to ensure you are happy with any changes to this policy. We may notify you of changes to this policy by email or by displaying a notification on our website homepage.
                </p>

                <h3><li>Your rights</li></h3>
                <p>
                    You may instruct us to provide you with any personal information we hold about you; provision of such information will be subject to the supply of appropriate evidence of your identity. For this purpose, we will usually accept a photocopy of your personal ID, a utility bill showing your current address, driver’s license or any government–issued ID.
                </p>
                <p>
                    We may withhold personal information that you request to the extent permitted by law.
                </p>

                <h3><li>Third party websites</li></h3>
                <p>
                    Our website includes hyperlinks to, and details of, third party websites and we have no control over, and are not responsible for, the privacy policies and practices of third parties. You are as such advised to read such third party websites’ privacy policy before proceeding with the use of such websites.
                </p>
                <h3><li>Cookies</li></h3>
                <p>
                    Our website uses cookies. A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.<br><br>
                    Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.<br><br>
                    Most browsers allow you to refuse to accept cookies but blocking all cookies will have a negative impact upon the usability of many websites. If you block cookies, you will not be able to use all the features on our website.<br><br>
                </p>
                <h3><li>Our details</li></h3>
                <p>
                    This website is owned and operated by GSM Abroad.<br><br>
                    Our principal place of business is at __________________.<br><br>
                    You can contact us by writing to the business address given above, by using our website contact form, by email to info@gmsabroad.com or by telephone on +573162855560.<br>
                </p>


            </ol>



        </div>
    </div>

    <?php
    footer();
 ?>
