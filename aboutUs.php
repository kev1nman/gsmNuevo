<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <div class="container">

                <div class="col-md-10 col-md-offset-1">
                    <h1>About US</h1>
                    <p>
                        GsmAbroad is a fast growing and leading cellphone remote unlocking and servicing technology provider in the telecommunications industry. We provide the best and easiest means to unlock your mobile. It's modern, no cables. All you need to do is enter the unlock code that will be generated for you and your device is unlocked without having to open your device. Hence, you have the freedom to use the services of any provider you want on your mobile device.<br><br>
                        Our aim is to deliver the top quality service in mobile phone remote unlocking at the best prices available around. Your satisfaction is our first priority and we are committed to provide our customers the best customer support services ever.
                    </p>

                </div>
            </div>

<?php
    footer();
 ?>
