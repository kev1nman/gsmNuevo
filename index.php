<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <!-- sect1 -->
            <section class="sect1">
                <!-- CONTAINER -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-6 col-md-6">
                                <!-- Titulo -->
                                <div class="col-md-12 text-center">
                                    <img class="img-responsive" src="img/logo.png" width="400px" alt="">
                                    <h1 class="textWhite">UNLOCK YOUR PHONE<br>in 3 easy steps</h1>
                                </div><!-- FIN / Titulo -->

                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <a href="#" class="btn videoIntro">
                                        <img class="img-responsive imgCenter" src="img/videoIntro.png" alt="">
                                    </a>
                                </div>

                                <!-- Comentarios -->
                                <div class="col-xs-12 col-sm-12 col-md-12 comentariosJack">

                                        <div class="scrollbar">
                                            <div class="handle" style="transform: translateZ(0px) translateX(0px); width: 71px;">
                                                <div class="mousearea"></div>
                                            </div>
                                        </div>


                                        <div class="frame" id="oneperframe" style="overflow: hidden;">
                                            <ul class="clearfix" style="transform: translateZ(0px) translateX(0px); width: 18256px;">
                                                <li class="active">
                                                    Jack on 04-05-16 <br>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span> <br>
                                                    Excelent!!! Great service
                                                </li>
                                                <li class="">
                                                    Jack on 04-05-16 <br>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span> <br>
                                                    Excelent!!! Great service
                                                </li>
                                                <li class="">
                                                    Jack on 04-05-16 <br>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span> <br>
                                                    Excelent!!! Great service
                                                </li>
                                                <li class="">
                                                    Jack on 04-05-16 <br>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span>
                                                    <span class="icon-star-full textVerde"></span> <br>
                                                    Excelent!!! Great service
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="controls center">
                                            <button class="btn btn-info prev disabled circleLeft" disabled=""><i class="icon-arrow-left2"></i></button>
                                            <button class="btn btn-info next circleRight"><i class="icon-arrow-right2"></i></button>
                                        </div>

                                </div><!-- FIN / Comentarios -->


                            </div>

                            <div class="col-sm-6 col-md-6 text-center">

                                <div class="phone">
                                    <div class="formPhone">
                                        <h2 class="getStarted">GET STARTED</h2>
                                        <form class="form text-center" action="#" method="post" role="form">
                                            <div>
                                                <div class="form-group">
                                                    <select class="form-control selectPhoneLeft" name="manufacture">
                                                        <option>Your phone manufacture</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <select class="form-control selectPhoneLeft" name="model">
                                                        <option>Your phone model</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <input class="form-control selectPhone" type="text" name="imei" value="" placeholder="Enter your imei_no">
                                                </div>

                                                <button type="submit" class="btn btn-default btn-lg btn-success unlockBtn">UNLOCK</button>

                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- FIN / CONTAINER -->

                <!-- Steps -->
                <div class="col-md-12 steps">
                    <div class="row">
                        <div class="col-xs-12 text-uppercase text-center">
                            <h1>3 Easy steps</h1>
                            <br>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                                <img class="img-responsive imgCenter" src="img/icon1.png" alt="">
                                <h3>1. Select your phone and fill in form</h3>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img class="img-responsive imgCenter" src="img/icon2.png" alt="">
                                <h3>2. We email you the code</h3>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img class="img-responsive imgCenter" src="img/icon3.png" alt="">
                                <h3>3. Enter the code into the phone and done</h3>
                            </div>
                        </div>
                    </div>
                </div><!-- Steps -->
            </section><!-- FIN / sect1 -->

            <!-- sect2 - CARRIES/MODELS/RBANDS -->
            <section class="sect2">
                <!-- CONTAINER -->
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 text-center popular">
                            <div class="col-xs-6 col-md-6 popularTittle popularTittleLeft">
                                <h2 class="carries activePopular" id="carries">Popular Carries</h2>
                            </div>
                            <div class="col-xs-6 col-md-6 popularTittle popularTittleRIght">
                                <h2 class="brands" id="brands">Popular Brands</h2>
                            </div>
                        </div>
                        <div class="populares text-center">

                            <!-- POPULAR CARRIES -->
                            <div class="col-md-12 text-center noPadding" id="ContentCarries">
                                <div class="col-md-12 sectionsContent">
                                    <div class="col-md-12"><!-- unlockBox -->
                                        <div class="col-md-12">
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/carries/1.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/carries/2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/carries/3.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/carries/4.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-2">
                                                <div class="unlockBox">
                                                    <img src="img/carries/5.jpg" alt="">

                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/carries/6.jpg" alt="">

                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/carries/7.jpg" alt="">

                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- FIN / unlockBox -->
                                </div>
                            </div><!-- FIN / POPULAR CARRIES -->

                            <!-- POPULAR BRANDS -->
                            <div class="col-md-12 noPadding" id="ContentBrands">
                                <div class="col-md-12 sectionsContent">
                                    <div class="col-md-12"><!-- unlockBox -->
                                        <div class="col-md-12">
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/1.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/3.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/4.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/5.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/6.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/7.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/8.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12">
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/9.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 col-md-3">
                                                <div class="unlockBox">
                                                    <img src="img/brands/10.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- FIN / unlockBox -->
                                </div>
                            </div><!-- FIN / POPULAR BRANDS -->
                        </div>

                        <div class="col-xs-12 col-md-12 text-center">
                            <img class="img-reponsive publicidad" src="img/moreP/2.jpg" alt="">
                            <img class="img-reponsive publicidad" src="img/moreP/1.jpg" alt="">
                        </div>


                    </div>
                </div><!-- FIN / CONTAINER -->
            </section><!-- FIN /sect2 - CARRIES/MODELS/RBANDS -->

            <!-- POPULAR MODELS -->
            <div class="col-md-12 models">
                <div class="col-md-12 text-center">
                    <h1 class="">POPULAR MODELS</h1>
                </div>

                <!-- Comentarios -->
                <div class="col-xs-12 col-sm-12 col-md-12 popularModels">

                        <div class="frame" id="popularModels" style="overflow: hidden;">
                            <ul class="clearfix" style="transform: translateZ(0px) translateX(0px); width: 18256px;">
                                <li class="active">
                                    <div class="col-xs-6 col-md-6 text-right">
                                        <img src="img/models/1.png" alt="">
                                    </div>
                                    <div class="col-xs-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>APPEL</h1>
                                            <a href="#" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span> UNLOCK</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="col-xs-6 col-md-6 text-right">
                                        <img src="img/models/2.png" alt="">
                                    </div>
                                    <div class="col-xs-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>LG</h1>
                                            <a href="#" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span> UNLOCK</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="col-xs-6 col-md-6 text-right">
                                        <img src="img/models/3.png" alt="">
                                    </div>
                                    <div class="col-xs-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>Motorola</h1>
                                            <a href="#" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span> UNLOCK</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="col-xs-6 col-md-6 text-right">
                                        <img src="img/models/4.png" alt="">
                                    </div>
                                    <div class="col-xs-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>Samsung</h1>
                                            <a href="#" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span> UNLOCK</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <div class="col-xs-6 col-md-6 text-right">
                                        <img src="img/models/5.png" alt="">
                                    </div>
                                    <div class="col-xs-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>Huawei</h1>
                                            <a href="#" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span> UNLOCK</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="controls center">
                            <button class="btn btn-info prev disabled circleLeft" disabled=""><i class="icon-arrow-left2"></i></button>
                            <button class="btn btn-info next circleRight"><i class="icon-arrow-right2"></i></button>
                        </div>

                </div><!-- FIN / Comentarios -->

            </div><!-- FIN / POPULAR MODELS -->

            <!-- sect3 - WHY USE GSM ABROAD-->
            <section class="sect3">
                <!-- CONTAINER-FLUID-->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center noPadding">
                            <h1 id="whyUse">Why use <span class="textAzul MyriadProBold">GSM<span class="textAzulClaro">ABROAD?</span></span></h1>
                                <div class="col-md-12">
                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                        <img src="img/azul1.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 text-left">
                                        <h2 class="textAzul">Lowest Prices Always</h2>
                                        <h3 class="text-justify borderBottom">
                                            If you have lower prices, Just contact us to negotiate our advertised prices for your needs. We always beat any real price.
                                        </h3>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                        <img src="img/azul2.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 text-left">
                                        <h2 class="textAzul">Wholesale Unlock</h2>
                                        <h3 class="text-justify borderBottom">
                                            GSM Abroad has unlocked millions of phones worldwide. We are your best source for your Unlock needs. We supply solutions for all type of customers
                                        </h3>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                        <img src="img/azul3.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 text-left">
                                        <h2 class="textAzul">Money Back Guarantee</h2>
                                        <h3 class="text-justify borderBottom">
                                            Don´t need to worry abput your money. If you meet service terms. we always grant full of your purchase
                                        </h3>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div><!-- FIN / CONTAINER-FLUID-->
            </section><!-- FIN / sect3 - WHY USE GSM ABROAD-->


            <!-- VIDEO - FEATURES -->
            <div class="video">
                <div class="row">

                    <div class="col-xs-12 text-center videoTittle">

                        <div class="col-xs-12 tittlesVideo">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h2 class="features activeVideo" id="features">Features</h2>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h2 class="videoTour" id="video">Video Tour</h2>
                            </div>
                        </div>

                        <div class="col-xs-12" id="ContentFeatures">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="video-responsive">
                                    <iframe  src="https://www.youtube.com/embed/0bd8qqYzRnw" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12" id="ContentVideo">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="video-responsive">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/fOYmgaGlRLQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div><!-- FIN / POPULAR MODELS -->


            <!-- CORPORATE -->
            <div class="col-md-12 corporateSect">

                <!-- sly - CORPORATE -->
                <div class="col-xs-12 col-sm-12 col-md-12 corporate">

                        <div class="frame" id="corporate" style="overflow: hidden;">
                            <ul class="clearfix" style="transform: translateZ(0px) translateX(0px); width: 18256px;">
                                <li class="active">
                                    <div class="col-xs-12 col-sm-6 col-md-6 text-center">
                                        <img class="corporateImg" src="img/corporate/1.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>Corporate</h1>
                                            <h3>
                                                Our servers are fully automated and ready to unlock any quantity of orders in a timely manner. We have best solutions and prices for wholesalers, recyclers and cellphone corporations.
                                            </h3>
                                            <a href="corporateCustomer.php" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span> Corporate phone unlock</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="col-xs-12 col-sm-6 col-md-6 text-center">
                                        <img class="corporateImg" src="img/corporate/2.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>FEATURES</h1>
                                            <h3>
                                                You can access to your account, from any computer, worldwide. Unlock by Code more than 900 different Network Providers, 255 Brands and 15,000 Cell Phones. We offer the lowest prices to get your business more profitable and competitive. We focus on offering fast turnaround time for delivery of services, instant in several cases. Keep track of each order for each of your customers.
                                                <!-- <div class="listaFeatures">
                                                    <ul>
                                                        <li>•	Setup sub-accounts for your employees and limit access as per your needs,</li>
                                                        <li>•	Personal & Global Statistics to plan the outcome of any Unlock request,</li>
                                                        <li>•	Incredible Developer API to automate your business needs,</li>
                                                        <li>•	First Class 24/7 Customer service by email.</li>
                                                    </ul>
                                                </div>-->
                                            </h3>
                                            <a href="webmaster.php" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span>
                                            Affiliate program cellphone unlock</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="col-xs-12 col-sm-6 col-md-6 text-center">
                                        <img class="corporateImg" src="img/corporate/3.jpg" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 unlockModels">
                                        <div class="unlockButton">
                                            <h1>SUPPLIERS</h1>
                                            <h3>
                                                Working with us will guarantee you the greatest exposure, reliable working and accounting tools, and most importantly of all, payment in timely fashion. We can pay you using PayPal, MassPay or Bank Transfer to any account anywhere in the world.
                                            </h3>
                                            <a href="suppliers.php" class="btn btn-default btn-lg unlockBtn2"><span class="icon-unlocked"></span>
                                            Affiliate program cellphone unlock</a>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>

                        <div class="controls center">
                            <button class="btn btn-info prev disabled circleLeft" disabled=""><i class="icon-arrow-left2"></i></button>
                            <button class="btn btn-info next circleRight"><i class="icon-arrow-right2"></i></button>
                        </div>

                </div><!-- FIN / sly - CORPORATE -->

            </div><!-- FIN / CORPORATE -->



            <!-- sec4 - What we do-->
            <div class="col-xs-12 whatWeDo">
                <!-- CONTAINER-->
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h1 class="OpenSansRegular"><b>What We Do</b></h1>
                            <p>
                                We are leaders in Cellphone Remote Unlocking services and will provide you with an Unlock service in a timely manner at the best price. With an efficient courteous and reliable customer service we build lasting relationships with our
                            </p>
                            <a href="WhyUs.php" class="btn btn-info">Learn more</a>
                        </div>
                    </div>
            </div><!-- FIN / sect6 - SLIDER-->

            <!-- sect6 -->
            <section class="col-xs-12 sect6">
                <!-- CONTAINER-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-5 text-center">
                            <h3 class="MyriadProBold textAzul">ACCEPTED PAYMENT METHODS</h3>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <div class="col-xs-12 col-md-12">
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/1.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/2.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/3.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/4.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/5.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/6.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/7.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/8.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/9.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/10.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"><img class="img-responsive center-block" src="img/payments/11.jpg" alt=""></div>
                                <div class="col-xs-4 col-sm-3 col-md-2 noPadding"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- FIN / CONTAINER-->
            </section><!-- FIN / sect6 - SLIDER-->

            <script>
            $("#carries").click(function(){
                $("#ContentBrands").hide(100);
                $("#brands").removeClass('activePopular');
                $("#ContentCarries").show(100);
                $("#carries").addClass('activePopular');
            });

            $("#brands").click(function(){
                $("#ContentBrands").show(100);
                $("#brands").addClass('activePopular');
                $("#ContentCarries").hide(100);
                $("#carries").removeClass('activePopular');
            });

            $("#features").click(function(){
                $("#ContentVideo").hide(100);
                $("#video").removeClass('activeVideo');
                $("#ContentFeatures").show(100);
                $("#features").addClass('activeVideo');
            });

            $("#video").click(function(){
                $("#ContentVideo").show(100);
                $("#video").addClass('activeVideo');
                $("#ContentFeatures").hide(100);
                $("#features").removeClass('activeVideo');
            });
            </script>
<?php
    footer();
 ?>
