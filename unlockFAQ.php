<?php
require_once("master.php");
cabecera();
?>

<!-- CONTENIDO -->
<div class="contenido">
    <div class="container">

        <div class="col-md-10 col-md-offset-1">
            <h1>Unlock FAQ</h1>
            <ol>
                <h3><li>Why should I unlock my phone?</h3></li>
                <p>
                    Opening your wireless has different advantages: It builds the exchange quality and attractiveness of your telephone by not limiting the exchange to a particular bearer, it permits the handset to work around the world (See What systems are perfect with
                    my telephone?), it kills allowing so as to mean charges you to change to a neighborhood transporter in the nearby area.
                </p>
                <h3><li>Is it Legal to unlock my phone?</h3></li>
                <p>
                    The Digital Millennium Copyright Act presented an exception on November 27, 2006 particularly allowing opening of cell gadgets. The exception was planned to terminate on October 27, 2009, however was reached out on a between time premise, in light of
                    the fact that the Register of Copyrights had not yet finished its triennial audit of DMCA exclusions. In July 2010, the Librarian of Congress broadened the DMCA exclusion for an additional 3 years.

                </p>
                <h3><li>Why is my phone locked?</h3></li>
                <p>
                    At the point when the price tag of the cell telephone to the shopper is financed with income from memberships, administrators must recover this venture before an endorser ends administration. With a specific end goal to secure their speculations, GSM
                    system suppliers actualizes a product highlight called SIM Lock on the handset to counteract use outside their system. Such sponsorships are worth more than a few hundred US dollars and in that is the reason bearers additionally charge
                    a contractually allowable charge; another structure to recover the appropriation speculation.

                </p>
                <h3><li>How do I know my phone is locked?</h3></li>
                <p>
                    Embed a SIM Card from an alternate bearer than the one your handset is utilizing. At the point when force up is finished the handset will show a message like those underneath in the event that it SIM bolted:

                </p>
                <ul>
                    <li>Enter Subsidy Code</li>
                    <li>Enter Unlock Code</li>
                    <li>Incorrect Sim Card</li>
                    <li>Insert Correct Sim Card</li>
                    <li>Invalid Smart Chip</li>
                    <li>Phone Restricted</li>
                    <li>Sim Network Unlock Code</li>
                    <li>Sim Network Unlock Pin</li>
                    <li>Special Code Required</li>
                    <li>Wrong Sim Card</li>
                </ul>


                <h3><li>What is a phone SIM lock?</li></h3>
<p>
It is a product ability incorporated with GSM telephones by cell telephone makers. System suppliers utilize this to confine the utilization of these telephones to particular nations and system suppliers.

</p>
<h3><li>What is a phone unlock code?</li></h3>
<p>
An Unlock Code is a numeric string. At the point when gone into a bolted handset it discharges the SIM lock limitation and permits the handset to be utilized on other GSM transporters.

</p>
<h3><li>What is an IMEI?</li></h3>
<p>
The IMEI (i.e. Worldwide Mobile Equipment Identity) number is utilized by a GSM system to recognize gadgets endeavoring to associate with them and consequently can be utilized for preventing a stolen telephone from getting to that system. It's utilized for recognizing the gadget and has no perpetual or semi-lasting connection to the endorser. The IMEI number incorporates data on the beginning, model, and serial number of the gadget.<br><br>
IMEI = 14 digits + 1 Check Digit <br>
IMEISV = 14 digits + 2 Digit Software Version Number <br>
IMEI + SV = 14 digits + 1 Check Digit + 2 Digit Software Version Number <br><br>
The organization of the IMEI is AA-BBBBBB-CCCCCC-D, (15 Digits) despite the fact that it may not generally be shown along these lines. The IMEISV drops the Luhn CD (i.e. Check Digit) for an extra two digits for the SVN (i.e. Programming Version Number), making the organization AA-BBBBBB-CCCCCC-EE. The IMEI+SV consolidate both the check digit and the extra two digits for the Software Version Number, making the configuration AA-BBBBBB-CCCCCC-D-EE.

</p>
<h3><li>Where do I find the IMEI #?</li></h3>
<p>
On numerous gadgets, the IMEI number can be recovered by entering *#06#, or is situated in the battery compartment. <br><br>
Android: Settings - > About Phone <br>
Blackberry: Options - > Status<br>
IOS: Settings - > General - > About<br>
Sony Ericsson (New Devices) : Options - > status.<br>
Sony Ericsson (Old Devices) : Dial * Right * Left * Left.

</p>
<h3><li>What is a phone SIM card?</li></h3>
<p>
SIM (i.e. Supporter Identity Module) card is one of the key components of GSM. It is a separable keen card containing the client's membership data and telephone directory. This permits the client to hold his or her data subsequent to exchanging handsets.

</p>
<h3><li>What ARE results of unlocking a phone?</li></h3>
<p>
An Unlock Code will SIM Unlock the handset from the first bolted system, to be utilized on other GSM systems. (See What Networks are perfect with my telephone?)

</p>
<h3><li>What are NOT results of unlocking a phone?</li></h3>
<p>
Opening won't include recurrence groups for utilization CDMA/IDEN systems, give client free administrations (e.g. Web, Talk, Text, Roaming, and Ringtones), evacuate Lost/Stolen Ban, expel client passwords from SIM card or handset (See what is a PUK Code?), and void your Warranty.

</p>
<h3><li>Phone shows 'unregistered SIM', what should I do?</li></h3>
<p>
By procedure of disposal you ought to be rapidly ready to restrict down the reason to either a bolted/inadequate telephone, or a defective SIM. In the event that the issue has all the earmarks of being a broken SIM visit your neared system supplier store to get a substitution SIM Card. In the event that your telephone shows this message then have a go at resetting you're versatile, turn your portable off, uproot the battery, and guarantee your SIM is safely set up. Turn your versatile on once more. In the event that despite everything you get this message attempt your SIM in another working telephone - if the SIM works fine in another telephone then the issue may be an inadequate SIM If different SIMs additionally won't work in your telephone then there is a handset deficiency, or it has been blocked.

</p>
<h3><li>Can I use the original phone network after unlocking?</li></h3>
<p>
Yes, for instance on the off chance that you open an AT&T phone; it will in any case work faultlessly on AT&T.

</p>
<h3><li>Is the phone unlocking permanent?</li></h3>
<p>
Yes, when the telephone has been opened with us, it will stay along these lines until the end of time.

</p>
<h3><li>Will a software update lock my phone?</li></h3>
<p>
No, OEM programming/OS upgrades ought not to bolt the telephone. Be that as it may, if the telephone does lock once more, the client can reuse the first bolt code officially obtained to open the telephone

</p>
<h3><li>Will my phone unlock code expire?</li></h3>
<p>
No, open codes are perpetual and don't terminate.

</p>
<h3><li>Will warranty repair lock my phone?</li></h3>
<p>
It relies on upon the level of repair done to the handset. In the event that the principle board cannot be repaired and should be supplanted, the new primary board will have an IMEI number separate from the one sent in for guarantee repair and another open code would be needed. On the off chance that the primary board is not supplanted ordinarily the telephone will stay opened, in the occasion it is not, and then the client can utilize the first open code that was bought for reuse.

</p>
<h3><li>What networks are compatible with my phone?</li></h3>
<p>
To focus good systems the client should check the recurrence's similarity band from their handset and fancied system.
GSM systems work in various diverse transporter recurrence ranges with most 2G GSM systems working in the 900 MHz or 1800 MHz groups and 3G systems 850 MHz and 1900 MHz groups. In uncommon cases the 400 and 450 MHz recurrence groups are doled out in a few nations in light of the fact that they were beforehand utilized for original frameworks. Most 3G systems in Europe work in the 2100 MHz recurrence band.
</p>
    <ul>
        <li>Africa, Asia, & Middle East</li>
        900 MHz {most broadly used} & 1800 MHz
        <li>Europe</li>
        900 MHz {most broadly used}, 1800 MHz, & 2100 MHz {3G}
        <li>North America:</li>
        USA 850 MHz & 1900 MHz
        Canada 850 MHz {Backup/Rural Areas}, 1900 MHz {Urban Areas}
        <li>South America:</li>
        Brazil 850 MHz, 900 MHz, 1800/1900 MHz<br>
        Costa Rica 1800 MHz<br>
        Ecuador & Panama 850 MHz (Exclusively)<br>
        Guatemala, El Salvador, & Venezuela 850 MHz and 900/1900 MHz<br>
        Peru 1900 MHz
    </ul>

<h3><li>Why do some unlock codes cost more than others?</li></h3>
<p>
The cost of unlocking a handset varies upon availability of the SIM unlock code. A few systems will discharge open codes endless supply of the telephone or necessities of the endorser. Commonly in these cases the value per opening is moderately low contrasted with different alternatives. In alternate examples the system won't discharge the open code in view of strict qualification necessities or select deals course of action with the maker or a lot of different reasons, then the open code must be acquired through the telephone's producer. Getting codes by means of the producer will by and large be more costly than through the system. Also all systems don't have the same qualification prerequisites and some never discharges open codes under any circumstances. At the point when this is the situation, all models regardless of the handset's age ought to be asked for through the producer.

</p>
<h3><li>What are a Carrier / Network phone unlock method?</li></h3>
<p>
At the point when utilizing a Carrier/Network open system the open code is gained specifically from the Carrier/Network (e.g. AT&T, T-Mobile). When you buy your open code with us, we will be able to waive a qualification's percentage necessities. This technique is generally the cheapest, however it just conveys, overall, 75% of requests effectively, the staying 25% must then be obtained through Factory/Manufacture system.

</p>
<h3><li>What are a Factory / Manufacturer unlock method?</li></h3>
<p>
At the point when utilizing a Factory or Manufacturer open system the code is obtained straightforwardly from the Factory or Manufacturer of the handset (e.g. HTC, LG). This strategy is typically more extravagant however conveys 95% of requests effectively.

</p>
<h3><li>What is a phone hard lock?</li></h3>
<p>
A Hard Lock is a changeless transporter lock. Your telephone will turn out to be hard bolted by endeavoring to enter an off base SIM open code a bigger number of times than your telephone permits, for all time bolting the telephone to the present transporter. Every telephone has distinctive breaking points with respect to unsuccessful endeavors before the gadget turns out to be hard bolted. The standard sum for HTC, Motorola, Nokia, and Samsung is 5 endeavors, regular sum for LG and Blackberry is 10 endeavors. The messages underneath are samples of what a Hard Locked telephone will show.
<ul>
<li>BlackBerry:</li>
(0 Left)<br>
Code Error, Please Wait...
<li>Huawei:</li>
SIMLOCK piece open reset KEY<br>
SIM system subnets open PIN
<li>HTC:</li>
You have attempted 5 times, please sit tight for timeout...
<li>LG:</li>
Open Attempt: 10 of 10
<li>Motorola:</li>
Contact Service<br>
Contact Service Provider<br>
Number of Attempts Remaining: 0<br>
Alter<br>
Hold up before Enter Special Code
<li>Nokia:</li>
Can't Undo Restriction<br>
Not Allowed<br>
This telephone just acknowledges SIMs from particular systems, and has been blocked.
<li>Pantech:</li>
Contact your client administration place for the unblock code.
<li>Samsung:</li>
Telephone Freeze
<li>Sony Ericsson/Xperia:</li>
Couldn't open system<br>
NCK (0)<br>
System (0)
</ul>
</p>
<h3><li>What is a PUK code?</li></h3>
A PUK (i.e. PIN Unlock Key) is obliged to open SIM cards that have gotten to be bolted after three progressive inaccurate PIN passages.
Note: If you enter the wrong PUK code 10 times in succession, your SIM card will be nullified, and you should buy another one. The telephone will show the accompanying blunder: "PUK blocked call administrator".
</ol>



</div>
</div>

<?php
footer();
?>
