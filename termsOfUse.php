<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <div class="container">

                <div class="col-md-10 col-md-offset-1">
                    <h1>Terms of Use</h1>
                    <p>
                        Your use of the GsmAbroad.com website, <a href="www.gsmabroad.com">www.gsmabroad.com</a> (hereinafter referred to as “GsmAbroad.com”) and services and tools are governed by the following terms and conditions as applicable to GsmAbroad.com. If you transact on GsmAbroad.com, you shall be subject to policies that are applicable to the website for such transaction. By mere use of the website, you shall be contracting with GsmAbroad.com and these terms and conditions constitute your binding obligations.<br><br>
                        For the purpose of these terms of use, wherever the context so require ‘You’ shall imply any natural or legal person who uses or has agreed to become a member of the Website by providing Registration data while signing up on the website as a Registered user using the computer systems of GsmAbroad.com.<br><br>
                        When you use any of the services provided by GsmAbroad.com, you will be subject to the rules, guidelines, policies, terms and conditions applicable to such service, and they shall be deemed to be incorporated into this Terms of use and shall be considered as part and parcel of this Terms of use. GsmAbroad.com reserves the right, at its sole discretion to change, modify, add or remove portions of these Terms of use, any time. It is your responsibility to check these Terms of use periodically for changes. Your continued use of the Site following the posting of changes will mean that you accept and agree to the changes. As long as you comply with these Terms of use, GsmAbroad.com grants you a personal, non-exclusive, non-transferrable, limited privilege to enter and use the Site.
                    </p>
                    <h3>MEMBERSHIP ELIGIBILITY</h3>
                    <p>
                        Use of the GsmAbroad.com Website is available only to persons who are above the age of 13 years. If you are under the age of 13 years, you shall not use or register as a member of GsmAbroad.com and shall not transact or use GsmAbroad.com website. However if you wish to use or transact on GsmAbroad.com, such use or transaction may be made by your legal guardian or parents. GsmAbroad.com reserves the right to terminate your membership and refuse to provide you with access to GsmAbroad.com if it is brought to GsmAbroad.com’s notice or if it is discovered that You are under the age of 13 years.
                    </p>
                    <h3>YOUR ACCOUNT AND REGISTRATION OBLIGATIONS</h3>
                    <p>
                        If you use GsmAbroad.com, you shall be responsible for maintaining the confidentiality of your User ID and Password and you shall be responsible for all activities that occur under your User ID and Password. You agree that if you provide any information that is untrue, inaccurate, not current or incomplete that GsmAbroad.com has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the this Terms of Use, GsmAbroad.com has the right to indefinitely suspend or terminate or block access of your membership with GsmAbroad.com and refuse to provide you with access to the Website.<br><br>
                        You also must notify us in writing immediately if you become aware of any unauthorized use of your account.
                    </p>
                    <h3>COMMUNICATIONS</h3>
                    <p>
                        When You use the Website or send emails or other data, information or communication to GsmAbroad.com, You agree and understand that You are communicating with GsmAbroad.com through electronic records and You consent to receive communications via electronic records from GsmAbroad.com periodically and as and when required. GsmAbroad.com may communicate with you by email or by such other mode of communication, electronic or otherwise.
                    </p>
                    <h3>LICENSE TO USE WEBSITE</h3>
                    <p>
                        You may view pages from our website in a web browser, download pages from our website for caching in a web browser, print pages from our website, stream audio and video files from our website and use our website services by means of a web browser.<br><br>
                        Except as expressly permitted by statement above or the other provisions of these terms and conditions, you must not download any material from our website or save any such material to your computer.<br><br>
                        Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.<br><br>
                        We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.
                    </p>
                    <h3>ACCEPTABLE USE</h3>
                    <p>
                        You must not:
                    </p>
                    <ul>
                        <li>use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;</li>
                        <li>use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</li>
                        <li>use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</li>
                        <li>conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;</li>
                    </ul>
                    <p>
                        You must not use data collected from our website to contact individuals, companies or other persons or entities.
                        You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete and non-misleading.<br><br>
                        If you abuse our services in any way, we may, in our sole discretion, limit, suspend, or terminate your user account and access to our Services, remove any special status associated with your account, delay or remove hosted content and take legal and/or technical steps to prevent you from using our Services.<br><br>
                        We encourage you to confirm your new accounts, and keep your account active as failure to do so might result to us cancelling your account. In addition, we reserve the right to terminate or refuse our Services to anyone for any reason at our discretion.
                    </p>
                    <h3>COPYRIGHT</h3>
                    <p>
                        All contents of this web site, including the design, text and graphics are owned by or licensed to GsmAbroad and are protected by copyright under the laws of Colombia and other countries. Apart from fair dealing for the purpose of personal use, private study, research, criticism or review as permitted under copyright legislation, you may not reproduce, transmit, adapt, distribute, sell, modify or publish or otherwise use any of the material on this web site without the prior written consent of GsmAbroad and where appropriate the relevant Partner business.
                    </p>
                    <h3>TRADEMARKS</h3>
                    <p>
                        This web site includes trademarks which are registered or the subject of pending applications or which are otherwise protected by law. You may not use these trademarks without the consent of GsmAbroad and where appropriate the relevant Partner business.
                    </p>
                    <h3>PRIVACY</h3>
                    <p>
                        We view protection of Your privacy as a very important principle. We understand clearly that you and Your Personal Information is one of our most important assets. Please review our Privacy Notice, which also governs your use of GsmAbroad Services, to understand our practices.
                    </p>

                    <h3>COPYRIGHT COMPLAINTS</h3>
                    <p>
                        GsmAbroad respects the intellectual property of others. If you believe that your work has been copied in a way that constitutes copyright infringement, please follow our Notice and Procedure for Making Claims of Copyright Infringement.
                    </p>

                    <h3>LIMITED WARRANTIES</h3>
                    <p>
                        We do not warrant or represent the completeness or accuracy of the information published on our website, that the material on the website is up to date or that the website or any service on the website will remain available.<br><br>
                        We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in these terms and conditions, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.<br><br>
                        To the maximum extent permitted by applicable law and subject to the next section, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the use of our website.
                    </p>
                    <p>
                        LIMITATIONS AND EXCLUSIONS OF LIABILITY
                    </p>
                    <p>
                        Nothing in a contract under these terms and conditions will:
                    </p>
                    <ul>
                        <li>limit or exclude any liability for fraud or fraudulent misrepresentation;</li>
                        <li>limit any liabilities in any way that is not permitted under applicable law; or</li>
                        <li>exclude any liabilities that may not be excluded under applicable law.</li>
                    </ul>
                    <p>
                        The limitations and exclusions of liability set out in this Section and elsewhere in a contract under these terms and conditions are subject to the above statement and govern all liabilities arising under that contract or relating to the subject matter of that contract, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in that contract.<br><br>
                        To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.<br><br>
                        We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.
                        We will not be liable to you in respect of any special, indirect or consequential loss or damage.<br><br>
                        You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).
                    </p>
                    <h3>BREACHES OF THESE TERMS AND CONDITIONS</h3>
                    <p>
                        Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may:
                    </p>
                    <ul>
                        <li>send you one or more formal warnings;</li>
                        <li>temporarily suspend your access to our website;</li>
                        <li>permanently prohibit you from accessing our website;</li>
                        <li>block computers using your IP address from accessing our website;</li>
                        <li>contact any or all of your internet service providers and request that they block your access to our website;</li>
                        <li>commence legal action against you, whether for breach of contract or otherwise; and/or</li>
                        <li>suspend or delete your account on our website.</li>
                    </ul>
                    <p>
                        Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).
                    </p>
                    <h3>VARIATION</h3>
                    <p>
                        We may revise these terms and conditions from time to time.<br><br>
                        The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions.<br><br>
                        If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.
                    </p>

                    <h3>THIRD PARTY RIGHTS</h3>
                    <p>
                        A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party. The exercise of the parties' rights under a contract under these terms and conditions is not subject to the consent of any third party.
                    </p>
                    <h3>SITE POLICIES</h3>
                    <p>
                        These terms and conditions, together with [our privacy and cookies policy], shall constitute the entire agreement between you and us in relation to your use of our website and shall supersede all previous agreements between you and us in relation to your use of our website.
                    </p>
                    <h3>APPLICABLE LAW</h3>
                    <p>
                        By using any GsmAbroad Service, you agree that the Federal Arbitration Act, applicable federal law, and the laws of Colombia, without regard to principles of conflict of laws, will govern these Conditions of Use and any dispute of any sort that might arise between you and GsmAbroad.
                    </p>
                    <h3>OUR DETAILS</h3>
                    <p>
                        This website is owned and operated by GSM Abroad.<br><br>
                        Our principal place of business is at __________________.<br><br>
                        You can contact us by writing to the business address given above, by using our website contact form, by email to info@gmsabroad.com or by telephone on +573162855560.
                    </p>


                </div>
            </div>

<?php
    footer();
 ?>
