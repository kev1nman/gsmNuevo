<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <div class="container">

                <div class="col-md-10 col-md-offset-1">
                    <h1>Hi! Thank you for ordering my gig</h1>
                    <p>
                        Here's what I need you to do before I can complete your order
                    </p>
                    <ol>
                        <h3><li>What is the website name?</li></h3>
                        <p>GsmAbroad</p>

                        <h3><li>What is the website URL?</li></h3>
                        <p>www.gsmabroad.com (currently being designed, you can view it here: www.gsmabroad.com/gsm)</p>

                        <h3><li>Is user registration required?</li></h3>
                        <p>For retail unlock purchases, no need to register. But for Wholesale unlock services, it’s needed to register an account.</p>
                        <h3><li>What information will be collected from users who register with the website?</li></h3>
                        <p>Name, email address, phone number, country. Billing information. * In the register form we should require more info just to make sure who is the real person doing that.</p>
                        <h3><li>What profile information is collected through the website?</li></h3>
                        <p>We need basic info as first authentication process, preventing cheaters to log in with us.</p>
                        <h3><li>What information will be collected from users who subscribe to website services,   email notifications and/or newsletters?</li></h3>
                        <p>Only email address.</p>
                        <h3><li>What can be purchased through the website: goods, services or both? What transactional data will be collected from users?</li></h3>
                        <p>Yes, all Unlock service will be able to purchase directly our website. We offer PayPal, crecit card payment, Skrill, Western Union, Money Gram. Mainly the required info is billing information in case of credit card paypment. Regarding payPal or Skrill it will be redirected to paypal/Skrill checkout which is API with our website. Western Union and money gram just need payment ID from customers…</p>
                        <h3><li>What personal information can a user publish through the website?</li></h3>
                        <p>None personal info is permitted to be publish in our website. We keep personal info private and safe.</p>
                        <h3><li>What other types of information might you collect?</li></h3>
                        <p>In the register form we should require more info or Methods to make sure who is the real person doing that.</p>
                        <h3><li>For what purposes will the website operator use personal information collected through the website?</li></h3>
                        <p>Only for authentication purposes. To protect us from fraud users.</p>
                        <h3><li>Will users have the opportunity to publish any personal information on the website?</li></h3>
                        <p>Yes, maybe replying in blog articles they will be able to publish personal info, but it will be out of our hands…</p>
                        <h3><li>Will the website undertake in this document not to supply any user's personal information to a third party for the purpose of third party direct marketing?</li></h3>
                        <p>No, we don’t sale/share personal Info. All will be stored in a private database from our hosting account.</p>
                        <h3><li>Will the website operator pass any personal information to any third party payment services provider?</li></h3>
                        <p>No, we don’t sale/share personal Info. All will be stored in a private database from our hosting account.</p>
                        <h3><li>What is the name of the payment services provider that processes website transactions? At what web address can the privacy policy of the payment services provider be found?</li></h3>
                        <p>We offer PayPal, crecit card payment, Skrill, Western Union, Money Gram. Mainly the required info is billing information in case of credit card paypment. Regarding payPal or Skrill it will be redirected to paypal/Skrill checkout which is API with our website. Western Union and money gram just need payment ID from customers…</p>
                        <p>We will need to search for each payment service provider privacy policy links…</p>
                        <h3><li>Is the website operator part of a group of companies in circumstance where one or more of those other group companies might need access to personal information collected by reference to this document?</li></h3>
                        <p>No.</p>
                        <h3><li>Does the website operator need a right to disclose personal data to business purchasers and potential business purchasers?</li></h3>
                        <p>No need. Every customer or purchaser personal data is 100% private and is not shareable or public anytime.</p>
                        <h3><li>Will users have the opportunity to publish personal information on the website?</li></h3>
                        <p>Repeated question… Same as question # 11</p>
                        <h3><li>Does the website operator have any specific policies in place regarding time periods for the deletion of personal data?</li></h3>
                        <p>If a user account is inactive for long time, the account will be disabled and all personal data will be deleted as well.</p>
                        <h3><li>Specify a category of personal data. Specify the date/time when that personal data will usually be deleted.</li></h3>
                        <h3><li>Will you ever contact users to notify them of changes to the document?</li></h3>
                        <p>Yes</p>
                        <h3><li>How will users be notified of changes to the document?</li></h3>
                        <p>Email notification</p>
                        <h3><li>What evidence of identity will you require before fulfilling a data protection subject access request?</li></h3>
                        <p>Maybe a legible photo of personal ID will be required.</p>
                        <h3><li>What types of cookies will be used on the website?</li></h3>
                        <p>Website is under development.</p>
                        <h3><li>What is the name of the cookie? For what purpose is the cookie used? Describe the purpose or purposes for which the cookie is used.</li></h3>
                        <h3><li>What is the name of the company, partnership, individual or other legal person or entity that owns and operates the website?</li></h3>
                        <p>GSM Abroad</p>
                        <h3><li>Is the website operator a company?</li></h3>
                        <p>No.</p>
                        <h3><li>In what jurisdiction is the website operator registered? What is the website operator's company registration number or equivalent? What is the website operator's registered address?</li></h3>
                        <p>We operate online only.</p>
                        <h3><li>Where is the website operator's head office or principal place of business?</li></h3>
                        <h3><li>What is the website operator's contact email address? What is the website operator's contact telephone number?</li></h3>
                        <p>info@gmsabroad.com +573162855560</p>
                        <p>Thank you, I look forward to providing you with an excellent end result!</p>

                    </ol>

                </div>
            </div>

<?php
    footer();
 ?>
