<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <div class="container">

                <div class="col-md-10 col-md-offset-1">
                    <h1>UNLOCK</h1>
                    <h2>Unlock Instructions Guide</h2>
                    <h4>
                        Here are some of the clear unlocking instructions for you to follow on some of phone brands:
                    </h4>
                    <ul>
                        <li>
                            <h3>ACER</h3>
                            <p>
                                Ensure that the “Wi-Fi” and “Airplane Mode” are turned off under a wireless connection. This is also in the case that your Acer is an Android device. You need to turn on the phone with the use of any non-accepted SIM card. Your phone will require you to unlock the code. You will also be required of entering the unlock code. And now, the phone is simply unlocked!
                            </p>
                        </li>
                        <li>
                            <h3>Blackberry</h3>
                            <p>
                                If ever you have received “CODE ERROR” right from the start, never worry of proceeding to the end part of the page. There is a section that lets you trouble shoot the issue and for you to proceed right away.
                                Never use all of the attempts in entering the Unlock code. If you will continue on doing this, your phone will still be Hardlocked. If ever you only have received a single unlock code, then there is no need for you to worry. The unlock code is also the MEP2 code or NCK code.
                                Other Blackberry phones like Koodo, Bell Blackberrys and Telus require the use of MEP4 or Prov Code. When you will enter the Unlock code into your Blackberry phone, you need to turn your phone on. Make use of a non-accepted simcard. Go the main menu and you will see the settings. Now, you need to focus your attention to the advanced options and your simcard.
                                You must type in the MEPD and type in the MEP. You must hold the ALT and also press the “2”. You will observe that another screen also pops up for your code. Enter the eight or sixteen digit unlock code that will be sent to you. When you have received the “Code Accepted”, your phone is already unlocked.
                            </p>
                        </li>
                        <li>
                            <h3>Alcatel</h3>
                            <p>
                                When unlocking the Alcatel, you will be required of turning on the phone without using any SIM card. And then, you will be required of composing the # 0000 and then the CODE number. Now, the UNLOCK code will be provided to you.
                                The next thing for you to do is to compose #0001 CODE #. You will also receive the unlock code and your phone is already unlocked. That’s just how easy it is for you to unlock it.
                            </p>
                        </li>
                        <li>
                            <h3>iPhones</h3>
                            <p>
                                When you have already obtained the “unlocked” or “activated” message, you need to follow the instructions carefully. Connect your phone to iTunes without a SIM card. You need to wait until iTunes has detected your phone. Afterwards, disconnect the phone and have it reconnected after ten seconds. Now, your phone is already unlocked.
                            </p>
                        </li>
                    </ul>
                    <h4>
                        When you will be unlocking your phones, ensuring that the “Wi-Fi” and Airplane Mode” is turned off is a must. This is especially true if the Motorola is an Android device. You need to take note that other Motorola models limit your attempt in entering the code. There are at least ten attempts when you will be doing this.
                        Now, you need to turn on your phone with the use of a non-accepted SIM card. Your phone will require you of the unlock code. Now, you need to enter it immediately. Expect that in an instant your phone is already unlocked.
                    </h4>

                </div>
            </div>

<?php
    footer();
 ?>
