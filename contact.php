<?php
    require_once("master.php");
    cabecera();
 ?>

        <!-- CONTENIDO -->
        <div class="contenido">
            <section class="registerBox">
                <div class="container">
                  <div class="col-md-6 col-md-offset-3">
                    <!-- contactBox -->
                    <form data-toggle="validator" role="form">
                      <h1>Contact</h1>
                      <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="myname@email.com" data-error="Bruh, that email address is invalid" focus required>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div class="col-md-6 noPadding">
                        <div class="form-group">
                          <label class="control-label" for="nombres">First name</label>
                          <input type="text" name="nombres" class="form-control" id="inputName" placeholder="First name" required>
                        </div>


                      </div>
                      <div class="col-md-6 noPaddingRight">

                        <div class="form-group">
                          <label for="telefono" class="control-label" for="telefono">Phone</label>
                          <input type="numbe" name="telefono" class="form-control" id="inputName" placeholder="Phone" required>
                        </div>

                      </div>

                      <div class="form-group">
                        <label for="mensaje" class="control-label" for="mensaje">Message</label>
                        <textarea name="mensaje" rows="8" cols="80" class="form-control" placeholder="Message" required></textarea>
                      </div>

                      <div class="form-group text-center">
                        <button type="submit" class="btn btn-success btn-lg">Suscribe</button>
                      </div>

                    </form>

                  </div>

                </div>

            </section>
<?php
    footer();
 ?>
