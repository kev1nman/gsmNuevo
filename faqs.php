<?php
    require_once("master.php");
    cabecera();
 ?>

<!-- CONTENIDO -->
<div class="contenido">
    <div class="container">

        <div class="col-md-10 col-md-offset-1">
            <h1>FAQ and Unlock Instructions</h1>
            <h4>At GSM Abroad, we are dedicated in sharing our knowledge and expertise of turning all people into experts at unlocking.</h4>
            <ul>
                <li><h5>Why Unlock My Phone?</h5></li>
                <p>
                    It is your utmost priority as a user to unlock your phone. This is due to the many varied benefits to get from it. When it comes time that you long to sell your phone, you already have increased its marketability and resell value. You will not also be
                    restricted to a specific carrier. You would never be required of paying for roaming charges in switching to a localized carrier in your region.

                </p>
                <li><h5>Can I Still Make Use of the Original Network of my Phone Right After Unlocking?</h5></li>
                <p>
                    Yes. You are still allowed of using the original network of your phone right after unlocking.

                </p>
                <li><h5>Why is it that my Phone is locked?</h5></li>
                <p>
                    GSM network providers are implementing a feature that is known as SIM lock right through the handset. The purpose is more on preventing the usage out of their network. With such subsidies, these cost more than hundred dollars. This is also the reason
                    why carriers are charging a termination fee as part of recouping the subsidy investment.

                </p>
                <li><h5>Will it be possible for the phone unlock to be permanent?</h5></li>
                <p>
                    With us, the phone unlock is already permanent. When your phone has already been unlocked with us, it will already stay that way.

                </p>
                <li><h5>Why is it that Other Unlock Codes are more expensive than others?</h5></li>
                <p>
                    You need to understand that unlocking a handset relies on the availability of the SIM for unlock code. Other networks are releasing unlock codes by first considering the requirements or phone of the subscribers. With this unlocking, the price to pay is
                    relatively low as compared to other choices. In some instances, the network does not release the code with the sales arrangement or eligibility requirements with the manufacturer. Thus, the unlock code should be obtained directly from
                    the phone manufacturer.

                </p>
                <li><h5>What do you mean by Phone Unlock Code?</h5></li>
                <p>
                    Unlock code is best understood as a numeric string. When this is already entered into the locked handset of your phone, the SIM lock restriction is already released. The handset can now be utilized with GSM carriers.

                </p>
                <li><h5>What do I expect from unlocking my phone?</h5></li>
                <p>
                    The SIM will be unlocked from the original locked network. As a user, you are now able to use it on any other GSM network.

                </p>
                <li><h5>What should I not expect from unlocking a phone?</h5></li>
                <p>
                    You need to take note that unlocking a phone does not add up to frequency bands to be used on IDEN/CDMA networks. Apart from it, you cannot expect that there will be free services to get from it. It will not also remove the passwords from the handset
                    or SIM card, or the stolen/lost ban.

                </p>
                <li><h5>Will the phone unlock code expire?</h5></li>
                <p>
                    The good thing is that the phone unlock code does not expire. It will already be permanent and will not expire.

                </p>
                <li><h5>If my phone has showed ‘unregistered SIM’, what must I do?</h5></li>
                <p>
                    You need to make an effort of narrowing down the main cause of a defective or locked phone. If you think that the issue is brought by a faulty sim, you need to contact your network provider. This is the only way for you to obtain a replacement SIM card.
                    If ever the message is displayed right through your phone, it should still be re-set. Your mobile phone needs to be turned off completely. The battery also needs to be removed and the SIM is to be secured in the right place. Turn the
                    mobile phone on again.

                </p>
            </ul>


        </div>
    </div>

    <?php
    footer();
 ?>
